---
title: "Internal directive on radiation protection for the operation of soft X-ray tubes"
author: "Damian Goeldi dgoeldi@phys.ethz.ch"
number-sections: true
format:
  pdf:
    papersize: a4
    include-in-header:
      - text: |
          \usepackage{siunitx}
          \usepackage{mhchem}
---

* University: ETHZ
* Department: D-PHYS
* Institute: IPA
* Research group: LEPP

# Scope

The scope of this directive comprises the operation of soft X-ray tubes inside a full protection system.

* BAG permit number: A-210270-79
* Issue date: 04.11.2022

## Declaration of equipment

* Type name: Hamamatsu N7599 Series Transmission Mode Soft X-Ray Tube
* Maximum rated voltage: $\qty{10}{\kilo\volt}$
* Maximum rated current: $\qty{0.2}{\milli\ampere}$
* Maximum rated power: $\qty{2}{\watt}$
* Full protection system: yes
* Location: HPZ C 31.1

# Relevant documents

* BAG permit for the use of ionising radiation
* Radiological Protection Act (RPA) of 22 March 1991
* Radiological Protection Ordinance (RPO) of 26 April 2017
* Verordnung des EDI über den Strahlenschutz bei nichtmedizinischen Anlagen zur Erzeugung ionisierender Strahlung (SnAV) of 26 April 2017
* Verordnung des EDI über die Aus- und Fortbildungen und die erlaubten Tätigkeiten im Strahlenschutz (Strahlenschutz-Ausbildungsverordnung) of 26 April 2017
* Internal operator's guide
* Hamamatsu datasheet

# Responsibilities

## Radiation protection expert (RPE){#sec-rpe}

* Name: Damian Goeldi
* E-Mail: dgoeldi@phys.ethz.ch
* Phone: +41 44 633 60 51
* Mobile: +41 78 859 03 28
* Radiation protection certification: I7 in 2023

The RPE arranges all necessary maintenance and repair works on the X-ray tube and its protection system. The RPE liaises with ETH Safety, Security, Health, and Environment radiation protection and ensures the proper reporting of any changes relevant to the BAG permit.

## ETH Safety, Security, Health, and Environment radiation protection (SSHE) point of contact{#sec-sshe}

* Name: Silke Kiesewetter
* E-Mail: silke-kiesewetter@ethz.ch
* Phone: +41 44 632 76 29
* Mobile: +41 79 728 16 62

# Radiation protection training

According to Art.\ 172, RPO the RPE must provide evidence of the successful participation in a radiation protection training recognised by the regulator.

Operation of equipment with a full or partial protection system requires a recognised training for scope I9. This scope does not require periodical refresher courses.

Operation of equipment without a full or partial protection system requires a recognised training for scope I7. This scope requires a refresher course every $\num{5}$ years. The refresher course must comprise at least $\num{8}$ teaching units, and is not subjected to official recognition.

The RPE equips all operators of the X-ray tube with the required knowledge.

# Occupationally radiation-exposed personnel

Operators of equipment with a full or partial protection system are not considered occupationally radiation-exposed personnel.

# Work technique

**Only personnel instructed by the RPE is allowed to operate the X-ray tube.**

* The X-ray tube is to be operated according to the operator's guide placed next to it.
* In particular it must NEVER be operated with any shielding removed.
* All screws MUST be installed prior to turning on power to the heater or anode high voltage.
* The operator is allowed to unscrew and remove the lid of the shielding box for setup changes AFTER turning off power to the heater and anode high voltage, AND securing them against restarting.

# Monitoring and maintenance

* Before every start-up the operator must check that the shielding box is fully and securely assembled. In particular, all screws must be checked twice.
* Before turning on the power supplies, the emergency off switch must be tested.
* After applying mains power to the power supplies, the proper operation of the warning light must be ensured.
* During operation, anode voltage and current must be continously monitored against the nominal and maximum ratings in the operator's guide.
* The X-ray tube does NOT require any maintenance with any shielding removed.

# Emergency instructions

## Emergency

* Press the emergency stop switch.
* Without electrical power there is no radiation danger.
* Do not open the shielding.
* Inform the RPE.
* Radiation incidents must be reported to SSHE (through the RPE).

## Malfuction (anomalous current or voltage readings)

* Turn off the tube gracefully according to the operator's guide.
* Do not open the shielding.
* Inform the RPE.

# Internal safety audits

The RPE periodically verifies compliance with this directive, as well as the technical and organisational safety measures.
Any observed defects must be fixed immediately.

# Permit and regulatory reporting

Any changes relevant the the BAG permit (e.g. address, RPE, tube, shielding, location, etc.) must be reported immediately to SSHE (see @sec-sshe).

For the operation of a new X-ray device, the RPE contacts SSHE for a new BAG permit.

# Bindingness and implementation

This is a directive of the BAG permit holder.
The listed terms are strictly binding.

This internal directive was approved and implemented on
\
\
\
by the research group leader.

\
\
\

:::: {layout-ncol=2}

::: {#leftcol}
Research group leader
:::

::: {#rightcol}
Radiation protection expert
:::

::::
