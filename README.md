# Safety at LEPP

This page contains addition LEPP-specific information complementary to the more general [Resources](#resources) below.


# Emergency

**Phone and fire extinguisher located next to elevator!**

[What to do](https://www.phys.ethz.ch/the-department/safety/emergency.html)


# General

Resources and contacts.


## Resources

- [D-PHYS safety](https://www.phys.ethz.ch/the-department/safety.html)
- [ETHZ Safey, Security, Health, and Environment (SSHE/SGU)](https://ethz.ch/en/the-eth-zurich/organisation/departments/safety-security-health-environment.html)
- [`How to play it safe in a lab` Moodle course](https://moodle-app2.let.ethz.ch/course/index.php?categoryid=103)
- [Waste disposal](https://www.phys.ethz.ch/the-department/safety/waste-disposal-at-d-phys.html)


## LEPP safety officer

[Damian Goeldi](https://lepp.ethz.ch/people/person-detail.MjgzODc4.TGlzdC80MDgwLC05ODA1MDcyOTE=.html)


## LEPP radiation protection officer

[Damian Goeldi](https://lepp.ethz.ch/people/person-detail.MjgzODc4.TGlzdC80MDgwLC05ODA1MDcyOTE=.html)


## LEPP laser safety officers

- [Damian Goeldi](https://lepp.ethz.ch/people/person-detail.MjgzODc4.TGlzdC80MDgwLC05ODA1MDcyOTE=.html)
- [Robert Waddy](https://lepp.ethz.ch/people/person-detail.MjkzODg2.TGlzdC80MDgwLC05ODA1MDcyOTE=.html)


## Reporting incidents or concerns

- Inform the [safety officer](#lepp-safety-officer) of every incident.
- As soon as there is no more immediate danger, please write down the events on the [LEPP ELOG](https://elog.psi.ch/elogs/Cold+Muonium+Mailing+List). Use the category `Safety` and the type `Incident`.
- Concerns can be reported to the [LEPP safety officer](#lepp-safety-officer) at any time. Ideally (but not necessarily), add them to the [LEPP ELOG](https://elog.psi.ch/elogs/Cold+Muonium+Mailing+List) using category `Safety` and type `Report` or `Note`, but **NOT** `Incident`.


# Onboarding

Read this section carefully, if you are new or need a refresher

- In case of an emergency, it is extremely important to call the internal emergency number, `888`, rather than the public emergency numbers. ETHZ employs 24/7 emergency first responders all across campus. They are much faster than the public first responders. `888` does **NOT** work from your mobile phone! It is very important to save `+41 44 342 11 88` as a contact on your mobile phone. This can literally save lifes!
- Complete the `How to play it safe in a lab` Moodle course (see [Resources](#resources)). This is a self enrolement course. For technical reasons, you need to select the one of the current month. There is an exam in the end. Upon completion, please send your certificate to the [safety officer](#lepp-safety-officer).
- TBD: Sign form


# Overview of danger in our labs

This section highlights important points about specific dangers in our labs. **Please also carefully read the *general information* under the given links**.


## Cryoliquids

- **NEVER** entrap cryoliquids in a closed volume: Danger explosion!
- **NEVER** enter a lift with a cryoliquid container inside: Danger of asphyxia (in case of lift malfunction)!
- Wear **watertight** gloves and shoes: Otherwise they can get soaked and cause frostbites.
- [General information](https://www.phys.ethz.ch/the-department/safety/cryoliquids.html)

We have a two-stage oxygen deficieny alarm in the lab. Here is how to respond.


### Pre-alarm

- `Gas alarm` signs start flashing. Note that they only flash inside the affected room and on the outside of the doors to the affected room. Unfortunately, the flashing signs are not very well visible.
- No audible alarm.
- ETHZ emergency desk is **NOT** contacted.
- What to do:
  - Close all gas sources (dewars, gas cylinders).
  - Evacuate lab.
  - Monitor oxygen level on panel outside main lab door.
  - After alarm stops, reenter lab and check for leaks.


### Alarm

- `Gas alarm` signs flashing.
- Audible alarm.
- ETHZ emergency desk is contacted **immediately**.
- What to do:
  - **Immediately** evacuate the lab and do not reenter.
  - In case of immediate danger, call **888**.
  - Call [safety officer](#lepp-safety-officer).
  - In case of false alarm (e.g. gas outlet directed at sensor), call **888** to inform them.


## Electricity

- All outlets in the lab are fused at 16A. Regular Swiss power cords only hold 10A. This is not an issue for a single device, but you have to be very careful when using a distributor (hence the daisy chain ban at PSI), not to overload the cord. It could catch fire without the breaker popping! This is especially important when using high-power devices such as heat guns. If needed, you can get 16A power strips and distributors from the PHYS shop. You can recognise them from the square pins.
- All outlets on the black circuit breaker panels are equipped with [Gound Fault Circuit Interrupters (GFCIs)](https://en.wikipedia.org/wiki/Residual-current_device). These offer some protection if you accidentally touch a live wire. The white power outlets on the walls are not necessarily protected. Hence in general prefer the outlets on the black panels.
- [General information](https://www.phys.ethz.ch/the-department/safety/electricity.html)


## Ionising radiation

- We currently only have an X-ray tube in the lab, no other radioactive sources.
- The X-ray tube can only be operated by people with authorisation from the [radiation protection officer](#lepp-radiation-protection-officer)
- [General information](https://www.phys.ethz.ch/the-department/safety/ionizing-radiation.html)


## Pressurised gases

- **NEVER** remove the valve protection cap of a gas cylinder unless it is secured against tipping: Danger of severe injury or death.
- Wear safety shoes when moving gas cylinders.
- [General information](https://www.phys.ethz.ch/the-department/safety/pressurised-gases.html)


## Lasers

- If you ever smell **garlic**, **immediately evacuate** the lab and contact the [laser safety officers](#lepp-laser-safety-officers).
- Lasers are only to be operated by people with authorisations from the [laser safety officers](#lepp-laser-safety-officers).
- [General information](https://www.phys.ethz.ch/the-department/safety/laser.html)


## Chemicals

- [General information](https://www.phys.ethz.ch/the-department/safety/toxins-and-chemicals.html)


## Fire

- [General information](https://www.phys.ethz.ch/the-department/safety/fire.html)


## Milling machine

- The milling machine can only be operated by people with authorisation from the [safety officer](#lepp-safety-officer).
- [General information](https://www.phys.ethz.ch/the-department/safety/mechanical-work.html)